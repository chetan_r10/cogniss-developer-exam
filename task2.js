const jsonfile = require("jsonfile");
const randomstring = require("randomstring");
const got = require("got");

const inputURL = "http://www.nactem.ac.uk/software/acromine/dictionary.py?sf=";
const outputFile = "output3.json";

var output = {};
console.log("getting 10 acronyms");
console.log("creating looping function");

output.definitions = [];

const getAcronym = function () {
  var acronym = randomstring.generate(3).toUpperCase();
  got(inputURL + acronym)
    .then((response) => {
      console.log("got data for acronym", acronym);
      console.log("add returned data to definitions array");
      if (output.definitions.length < 10 && response.body.length > 3) {
        console.log("calling looping function again");
        definitions.push(JSON.parse(JSON.stringify(response.body)));
        return getAcronym();
      }
    })
    .catch((err) => {
      console.log(err);
    });
};
// console.log("calling looping function");
getAcronym();
console.log(output.definitions);

console.log("saving output file formatted with 2 space indenting");
jsonfile.writeFile(outputFile, output, { spaces: 2 }, function (err) {
  console.log("All done!");
});
