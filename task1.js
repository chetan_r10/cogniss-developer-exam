const randomstring = require("randomstring");
const file = require("jsonfile");

const inputFile = "./input2.json";
const outputFile = "./output2.json";

/*
    read the data from input2.json file and store in a variable
*/
let rawData = file.readFileSync(inputFile);

let fileData = { email: [] };

/*
    1. using map function to access each element from names property.
    2. generate the required string to append random 5 characters and @gmail.com
    3. push the string to the email property
*/
rawData.names.map((x) => {
  const gmail = "@gmail.com";
  let tempString = x + randomstring.generate(5) + gmail;
  fileData.email.push(tempString);
});

/*
    write to the output2.json file
*/
file
  .writeFile(outputFile, fileData)
  .then((res) => {
    console.log("Write Complete");
  })
  .catch((err) => {
    console.error(err);
  });
